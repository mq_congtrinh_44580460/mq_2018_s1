import nltk 
nltk.download('punkt')
nltk.download('gutenberg')
nltk.download('brown')

emma = nltk.corpus.gutenberg.raw('austen-emma.txt')
toksents = [nltk.word_tokenize(s) for s in nltk.sent_tokenize(emma)]
tagged = nltk.pos_tag_sents(toksents, tagset="universal")
result = []
pos_list = ['DET', 'NOUN']
for pos in pos_list:
	count = 0
	for tag in tagged:
		count = count + len([w for (w,t) in tag if t == pos])
	result.append(count)
print(result)

