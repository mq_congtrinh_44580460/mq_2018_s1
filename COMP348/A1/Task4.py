import nltk
nltk.download('punkt')
nltk.download('gutenberg')
nltk.download('brown')
import re

regexp = re.compile('.*(first|second|third|fifth|eigth|ninth|twelfth)$')
regexpnum = re.compile('.*(st|nd|rd|th)$')
regexpth = re.compile('.*(th)$')
regexpieth = re.compile('.*(ieth)$')


def annotateOD(listoftokens):
    """Annotate the ordinal numbers in the list of tokens
    >>> annotateOD("the second tooth".split())
    [('the', ''), ('second', 'OD'), ('tooth', '')]
    """
    tagged = nltk.pos_tag(listoftokens, tagset = 'universal')
    result = []
    for (w, t) in tagged:
        if regexpnum.match(w) and (t == 'NUM'):
            result.append((w, 'OD'))
        elif regexp.match(w):
            result.append((w, 'OD'))
        elif regexpth.match(w):
            temp = w[:-2]
            temp = nltk.word_tokenize(temp)
            tag = nltk.pos_tag(temp, tagset = 'universal')
            if tag[0][1] == 'NUM':
                result.append((w, 'OD'))
            else:
                result.append((w, ''))
        elif regexpieth.match(w):
            temp = re.sub('ieth$', 'y', w)
            temp = nltk.word_tokenize(temp)
            tag = nltk.pos_tag(temp, tagset = 'universal')
            if tag[0][1] == 'NUM':
                result.append((w, 'OD'))
            else:
                result.append((w, ''))
        else:
            result.append((w, ''))
    return result


# DO NOT MODIFY THE CODE BELOW

def compute_f1(result, tagged):
    assert len(result) == len(tagged) # This is a check that the length of the result and tagged are equal
    correct = [result[i][0] for i in range(len(result)) if result[i][1][:2] == 'OD' and tagged[i][1][:2] == 'OD']
    numbers_result = [result[i][0] for i in range(len(result)) if result[i][1][:2] == 'OD']
    numbers_tagged = [tagged[i][0] for i in range(len(tagged)) if tagged[i][1][:2] == 'OD']
    if len(numbers_tagged) > 0:
        r = len(correct)/len(numbers_tagged)
    else:
        r = 0.0
    if len(numbers_result) > 0:
        p = len(correct)/len(numbers_result)
    else:
        p = 0.0
    return 2*r*p/(r+p)

if __name__ == "__main__":
    import doctest
    doctest.testmod()
    nltk.download('brown')
    tagged = nltk.corpus.brown.tagged_words(categories='news')
    words = [t for t, w in tagged]
    result = annotateOD(words)
    f1 = compute_f1(result, tagged)
    print("F1 score:", f1)
