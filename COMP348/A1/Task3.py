import nltk 
nltk.download('punkt')
nltk.download('gutenberg')
nltk.download('brown')

import re
VC = re.compile('[aeiou]+[^aeiou]+', re.I)
def count_syllables(word):
    return len(VC.findall(word))

emma = nltk.corpus.gutenberg.raw('austen-emma.txt')
tokens = nltk.word_tokenize(emma)
sents = nltk.sent_tokenize(emma)
count = 0
for token in tokens:
	count = count + count_syllables(token)
result = 206.835 - 1.015*(len(tokens)/len(sents)) - 84.6*(count/len(tokens))
print(result)
